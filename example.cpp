#include "coit.h"
#include <iostream>

int main()
{
    std::cout << coit::fg::LightRed << "I'm red\n";
    std::cout << coit::bg::Blue  << "I'm red on blue"<< coit::bg::Default<<"\n";
    std::cout << coit::bg::Red << coit::fg::Magenta << "I'm magenta on blue"<< coit::bg::Default<<"\n";
    
    std::cout << "Magenta ?\n";

    std::cout << coit::tocolorized("a fast Cyan text printer that reinit default after that\n",coit::fg::Cyan);
    std::cout << coit::tocolorized("a fast Grey text printer that reinit default after that",coit::fg::DarkGray,coit::bg::LightGray)<<"\n";
    std::cout << coit::tocolorized("default ?\n",coit::fg::Red,coit::bg::Default,coit::fmt::Italics);
    std::cout << coit::fmt::Default << "return to default " <<  coit::fmt::Bold << coit::fg::Cyan << "Bold Cyan"<<"\n";
    
    return 0;
}