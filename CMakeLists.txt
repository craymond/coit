cmake_minimum_required(VERSION 3.16)
project(coit VERSION 0.1.0 DESCRIPTION "header only c++ library that provides facilities to manipulate/color text in terminal"
HOMEPAGE_URL "https://gitlab.inria.fr/craymond/coit")
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_executable(example example.cpp)
target_compile_options(example PRIVATE -Wall)

