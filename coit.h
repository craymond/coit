#ifndef COIT_H
#define COIT_H
#include <iostream>
#include <sstream>

struct coit
{

    public:
    enum class fmt
    {
        Default = 0,
        Bold = 1,
        Dim = 2,
        Italics = 3,
        Underlined = 4,
        Blink = 5,
        Reverse = 7,
        Hidden = 8
    };

    enum class fg
    {
        Default = 39,
        Black = 30,
        Red = 31,
        Green = 32,
        Yellow = 33,
        Blue = 34,
        Magenta = 35,
        Cyan = 36,
        LightGray = 37,
        DarkGray = 90,
        LightRed = 91,
        LightGreen = 92,
        LightYellow = 93,
        LightBlue = 94,
        LightMagenta = 95,
        LightCyan = 96,
        White = 97
    };

    enum class bg
    {
        Default = 49,
        Black = 40,
        Red = 41,
        Green = 42,
        Yellow = 43,
        Blue = 44,
        Megenta = 45,
        Cyan = 46,
        LightGray = 47,
        DarkGray = 100,
        LightRed = 101,
        LightGreen = 102,
        LightYellow = 103,
        LightBlue = 104,
        LightMagenta = 105,
        LightCyan = 106,
        White = 107
    };
    struct text_cfg
    {        
        fg _fg;
        bg _bg;        
        fmt _fmt;
        
        constexpr text_cfg(const fg pfg,const bg pbg=bg::Default,const fmt pfmt=fmt::Default) : _fg(pfg),_bg(pbg),_fmt(pfmt){}
        constexpr text_cfg() : _fg(fg::Default),_bg(bg::Default),_fmt(fmt::Default){}
  
    };
    
    inline static text_cfg actual_cfg{};
   
    static std::string tocolorized(const std::string& s,const fg fmode, const bg bmode=bg::Default,const fmt fmtmode=fmt::Default);
};

inline std::ostream& operator<<(std::ostream& out,const coit::text_cfg& c)
{
        return out << "\033[" << static_cast<int>(c._fmt)<<";"<< static_cast<int>(c._fg)<<";"<< static_cast<int>(c._bg) << "m";
}

inline std::ostream& operator<<(std::ostream& out,const coit )
{
        return out <<coit::actual_cfg;
}

inline std::ostream& operator<<(std::ostream& out,const coit::fmt code)
{
    coit::actual_cfg._fmt=code;
    if(code==coit::fmt::Default)
    {
        coit::actual_cfg._fg=coit::fg::Default;
        coit::actual_cfg._bg=coit::bg::Default;
        return out << "\033["<<static_cast<int>(coit::fmt::Default)<<"m";
    }
    return out << coit();
}

inline std::ostream& operator<<(std::ostream& out,const coit::fg code)
{
    coit::actual_cfg._fg=code;
    return out << coit();
}

inline std::ostream& operator<<(std::ostream& out,const coit::bg code)
{
    coit::actual_cfg._bg=code;
    return out << coit();
}

inline std::string coit::tocolorized(const std::string& s,const coit::fg fmode, const coit::bg bmode,const coit::fmt fmtmode)
{
    std::ostringstream out;
    out << "\033["<<static_cast<int>(fmtmode)<<";"<< static_cast<int>(fmode)<<";"<< static_cast<int>(bmode)<<"m"<< s << coit();
    return out.str();
}

#endif
